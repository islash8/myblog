---
title: "Week Review"
date: 2018-06-14T00:05:55+02:00
draft: false
---
Oh, Hi Mark :"D, Didn't see you there :"D /*meme-ing to myself, really nice* :"D/</br>
Anyways, This is should be a quick weekly review, just to track things and keep it saved for future uses :| !! </br>

* Been working on Face recognition project and had been able to implement it on linux using the Haar cascade algorithm. the problems I faced are some of the libraries weren't added to the path variables (which I still don't know how to add them), but luckly I found some solutions where all you need is just download the libraries needed using Pacman commands (on arch). and here is the link of the tutorial I used for the code: https://www.youtube.com/watch?v=PmZ29Vta7Vc </br>

* Also, been working on ARM drivers course, cause I need to revise the stuff I studied in the previous summer diploma. Borrowed the tiva c kit from a friend, and struggled for a while to run a simple code on it, tried with eclipse but it didn't work cause there was peripheral files that need to be copied to the project file, and I didn't know how to do that and neither the course explained it cause he was working on a different board.</br>
Anyways, I found a way where you can just download code composer from the TI website, then start a new project normally, and then paste a simple blinking code which I'll provide the link for. And luckly it worked.</br>
Here are some useful links:</br>
https://technologyrealm.blogspot.com/2015/03/tiva-arm-development-in-linux-with.html </br>
http://chrisrm.com/howto-develop-on-the-ti-tiva-launchpad-using-linux/ </br>
https://github.com/sphanlung/TivaC/blob/master/LedBlink.c </br>
Another minor issue is that I noticed that you don't need to include the tm4c header file, in fact if you included it, the project will not compile as it
wasn't added from the beginning in the project workspace. </br>

* Also, since microsoft bought github, I decided to move this blog repo to gitlab, although I'm a noob and don't have that much of important repos, but It's good to try new things yourself, It teaches you more along the way. I wan't to try things and deploy the blog from anywhere I want. </br>

* I'm working on a linux introduction course, learned punch of stuff, navigating through the system and so on. also learned how to use .desktop files and where to put them in your file system. And used it to install punch of programs like eclipse, messenger. </br>
Here is a link for the course: </br>
https://www.youtube.com/watch?v=nLa6jAbULe8&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK&index=9 </br>

* Also, looked at an emacs tutorial, and didn't get much far in it cause it's a little bit complicated for my use, and the key-bindings are really hard to remember, needs a lot of practice, so I've decided to stick with what I know and works way more efficient which is the editor I'm writing on now (Atom), It's really brilliant, and I also can use the live markdown preview as I'm doing now writing this post :).

* Working on a business startup idea, with a friend of mine, who introduced me to another one of his friends, so we are currently 3 working on a startup :"D. It's a really good idea, but we are struggling a bit with launching and so on since they have their final year exam, so we have to postpone this project for now. 
