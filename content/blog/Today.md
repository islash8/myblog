---
title: "Today"
date: 2018-06-08T03:44:08+02:00
draft: false
---


Today :D, marking the 7th of June 2018, I know you can read the date of the blog entry, but i prefer writing it to emphasis how important today is. Today the beginning of an end to a strange yet remarkable story (well, I can't find the most suitable word to define what was it, but it was a **thing**). A story of a young man who dreamed going to an engineering school, so that's what he did. Anyways, we are not going to walk-through every moment of every year of it nor I'm going to mention the specifics of it cause that's kinda personal, but It was a painful journey, and I'm not really sure if I should be happy going through it or angry, frustrated maybe depressed. but one thing I'm sure of, which is I'm beyond happy and grateful finishing it. Here I learned a lot, fall a lot, and hopefully that I will always keep going forward on my way, trying to remember both the good sentiments and the bad ones, which will keep teaching me throughout the becoming years. It's still not yet an official graduation, but It's a start.</br>
Although this is the sunset of my college journey, It's the dawn of my career journey. Somehow I know that this phase or journey or whatever the name may be, won't be easier than the one before it. But I've faith that I'll get through eventually.
Today is the start of my freedom of choices, where I choose what to learn or what path to take.Today is the start of freedom of thought, to think and act on what you see fit, not just to do what everyone is doing goes that's what is going on and you need to keep up with it in order not to fall behind. Maybe I'm deceived That this is the case, but i have hope that if I think it and believe it, it will come to reality.</br>
Today is the **scratch** or the mark of a new beginnings, a new approach, a new mindset, new dreams, new self .......
