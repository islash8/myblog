---
title: "UART"
date: 2018-07-12T01:53:28+02:00
draft: true
---

Here is a quick view for how to get started with UART protocol, I used the Tiva C launchpad which uses the TM4C123GH6PM MCU. <br>
Firstly, We will need a serial terminal to communicate with the controller in order to send/receive information serially using the UART protocol. If you are working with a windows OS, you will need to download serial terminal which there are a lot of if you search online. If you are using a linux OS like myself, you will need to download a terminal called "PuTTY", if you are on arch linux you will only need to put this command in your terminal:
```
sudo pacman -S PuTTY
```
or just use the package manager directly :"D.<br>
After that you will need to know which serial port your launchpad is connected to, if you are using a windows OS, you can know which COM port using the device manager, if you are using linux, you can do so by using this command:
```
ls /dev/tty
```
then a list of tty devices will appear, all you need to do, is to see the list before and after plug-ing in your launchpad, mine was /dev/ttyACM0. <br>
here is the link i used to know how to deal with PuTTY: [Putty tutorial](https://www.youtube.com/watch?v=dO-BMOzNKcI)
<br>

#### Now, to the coding part:
1. you need to start the clock for the UART and the GPIO port used, which in our case was UART0 and GPIO Port A.
2. Now we need to start initializing the UART registers, by disabling the UART first then initialize the baud rate value in the integer and fraction divisor. Then use the system clock as the clock source for the UART using CC register, after that use the Line control register, and finally start the UART using the same control register we used in the beginning to disable the UART.
3. initialize the GPIO port, by enabling the digital function, then enable the alternate function for the pins used for the UART using the AFSEL register. Lastly, using the PCTL register we will need to specify which alternate function we need. This register is consists of bit fields, each 4 digits represent a single pin the same port. so we need to put 0x11 value in it. as we will need the first two pins in PORT A.
4. The transmit function consists of 2 statements, one for checking on the FR status register if the transmitter is empty, when it's empty, it goes to transfer data to UART which initializes a transmitter process.

Here is [source code for TX](https://github.com/islash8/UART_TX) and here is [source code for RX](https://github.com/islash8/UART_RX)
