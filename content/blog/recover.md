---
title: "Recover"
date: 2019-02-11T19:22:19+02:00
draft: false
---

When I tried to remove ubunutu, cause I had a problem making the root filesystem too small (20 GB), so it cause me a problem of not booting cause insufficient amount of space to store the temporary files needed for the booting process.<br>
Anyways, I completely removed ubuntu and tried to install manjaroo i3, when I rebooted the system, the grub menu failed to boot the manjaroo linux, and ran a black grub rescue screen. After that I tried to fix this problem by removing the boot partition and that removed the windows boot partition with it :"D. <br>
To recover the windows boot, you will have to burn an iso image for windows, and boot from it the recover screen, go to the command prompt and run listdisk, choose the "listdisk" command, and choose the partition which has the OS itself, and run a recover boot command which I can't remember, but it was a nice short sad adventure that I hope it won't happen again. :"D <br>

After that I reinstalled the manjaroo, but to make it a dual boot, you will have to burn the image itself as a GPT image and use the dd command, if ISO method were to be used, the flash drive won't boot, and probably will display the failed to boot due to security issue. <br>

